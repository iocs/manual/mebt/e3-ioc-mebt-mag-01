require essioc
require caenelsmagnetps, 1.2.2+1
require magnetps, 1.1.0

############################################################################
# ESS IOC configuration
############################################################################
epicsEnvSet("IOCNAME", "MEBT-010:SC-IOC-005")
epicsEnvSet("IOCDIR", "MEBT-010_SC-IOC-005")
iocshLoad("$(essioc_DIR)/common_config.iocsh")

############################################################################
# MEBT Magnets IP addresses
############################################################################
iocshLoad("$(E3_CMD_TOP)/ngps_ips.iocsh")
#
#
############################################################################
# Load conversion break point table
############################################################################
dbLoadDatabase("MEBT-010_BMD-QV-001.dbd", "$(magnetps_DB)/", "")
dbLoadDatabase("MEBT-010_BMD-QH-002.dbd", "$(magnetps_DB)/", "")
dbLoadDatabase("MEBT-010_BMD-QV-003.dbd", "$(magnetps_DB)/", "")
dbLoadDatabase("MEBT-010_BMD-QH-004.dbd", "$(magnetps_DB)/", "")
dbLoadDatabase("MEBT-010_BMD-QH-005.dbd", "$(magnetps_DB)/", "")
updateMenuConvert
#
#
############################################################################
# MEBT Quadrupole 1 -> QV-01
############################################################################
epicsEnvSet(PSUNIT, "PwrC-PSQV-001")
epicsEnvSet(PSPORT, "Q-01")
iocshLoad("$(caenelsmagnetps_DIR)/caenelsngps.iocsh", "Ch_name=NGPS$(PSPORT), IP_ADDR=$(PSQV1_IP), P=MEBT-010:, R=$(PSUNIT):")
afterInit('seq program_snl "P=MEBT-010:, R=PwrC-PSQV-001:Wave-"')
#
epicsEnvSet(CONVERSIONTABLE, "MEBT-010_BMD-QV-001")
dbLoadRecords("$(magnetps_DB)/magnetps.template", "P=MEBT-010:, R=$(PSUNIT):, READCURPV=Cur-R, BPT=$(CONVERSIONTABLE), EGU=T/m, ADEL=0")
#
#
############################################################################
# MEBT Quadrupole 2 -> QH-02
############################################################################
epicsEnvSet(PSUNIT, "PwrC-PSQH-002")
epicsEnvSet(PSPORT, "Q-02")
iocshLoad("$(caenelsmagnetps_DIR)/caenelsngps.iocsh", "Ch_name=NGPS$(PSPORT), IP_ADDR=$(PSQH2_IP), P=MEBT-010:, R=$(PSUNIT):")
afterInit('seq program_snl "P=MEBT-010:, R=PwrC-PSQH-002:Wave-"')
#
epicsEnvSet(CONVERSIONTABLE, "MEBT-010_BMD-QH-002")
dbLoadRecords("$(magnetps_DB)/magnetps.template", "P=MEBT-010:, R=$(PSUNIT):, READCURPV=Cur-R, BPT=$(CONVERSIONTABLE), EGU=T/m, ADEL=0")
#
#
############################################################################
# MEBT Quadrupole 3 -> QV-03
############################################################################
epicsEnvSet(PSUNIT, "PwrC-PSQV-003")
epicsEnvSet(PSPORT, "Q-03")
iocshLoad("$(caenelsmagnetps_DIR)/caenelsngps.iocsh", "Ch_name=NGPS$(PSPORT), IP_ADDR=$(PSQV3_IP), P=MEBT-010:, R=$(PSUNIT):")
afterInit('seq program_snl "P=MEBT-010:, R=PwrC-PSQV-003:Wave-"')
#
epicsEnvSet(CONVERSIONTABLE, "MEBT-010_BMD-QV-003")
dbLoadRecords("$(magnetps_DB)/magnetps.template", "P=MEBT-010:, R=$(PSUNIT):, READCURPV=Cur-R, BPT=$(CONVERSIONTABLE), EGU=T/m, ADEL=0")
#
#
############################################################################
# MEBT Quadrupole 4 -> QH-04
############################################################################
epicsEnvSet(PSUNIT, "PwrC-PSQH-004")
epicsEnvSet(PSPORT, "Q-04")
iocshLoad("$(caenelsmagnetps_DIR)/caenelsngps.iocsh", "Ch_name=NGPS$(PSPORT), IP_ADDR=$(PSQH4_IP), P=MEBT-010:, R=$(PSUNIT):")
afterInit('seq program_snl "P=MEBT-010:, R=PwrC-PSQH-004:Wave-"')
#
epicsEnvSet(CONVERSIONTABLE, "MEBT-010_BMD-QH-004")
dbLoadRecords("$(magnetps_DB)/magnetps.template", "P=MEBT-010:, R=$(PSUNIT):, READCURPV=Cur-R, BPT=$(CONVERSIONTABLE), EGU=T/m, ADEL=0")
#
#
############################################################################
# MEBT Quadrupole 5 -> QH-05
############################################################################
epicsEnvSet(PSUNIT, "PwrC-PSQH-005")
epicsEnvSet(PSPORT, "Q-05")
iocshLoad("$(caenelsmagnetps_DIR)/caenelsngps.iocsh", "Ch_name=NGPS$(PSPORT), IP_ADDR=$(PSQH5_IP), P=MEBT-010:, R=$(PSUNIT):")
afterInit('seq program_snl "P=MEBT-010:, R=PwrC-PSQH-005:Wave-"')
#
epicsEnvSet(CONVERSIONTABLE, "MEBT-010_BMD-QH-005")
dbLoadRecords("$(magnetps_DB)/magnetps.template", "P=MEBT-010:, R=$(PSUNIT):, READCURPV=Cur-R, BPT=$(CONVERSIONTABLE), EGU=T/m, ADEL=0")
#
#
iocInit()
